import { Component } from '@angular/core';
import Student from '../../entity/student';
import { FormGroup,FormControl,FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from 'src/app/service/student-service';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent {
  constructor(private fb: FormBuilder ,private studentService: StudentService, private router:Router){ }
  submit(){
    this.studentService.saveStudent(this.form.value)
       .subscribe((student)=>{
           this.router.navigate(['/detail',student.id+1]);
       },(error)=>{
         alert('could not save value');
       });
  }
  students: Student[];
  form = this.fb.group({
    id: [''],
    studentId: [''],
    name: [''],
    surname: [''],
    gpa: [''],
    image: [''],
    featured: [''],
    penAmount: [''],
    description: ['']
  });
  upQuantity(student: Student) {
   this.form.patchValue({
     penAmount:+this.form.value['penAmount']+1});
  }

  downQuantity(student: Student) {
    if (+this.form.value['penAmount'] > 0) {
      this.form.patchValue({
        penAmount:+this.form.value['penAmount']-1});
    }
  }
  get diagnostic(){
    console.log(this.form.value)
     return JSON.stringify(this.form.value);
  }

}
